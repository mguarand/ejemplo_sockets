#include "csapp.h"

void echo(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		echo(connfd);
		printf("Luego de funcion echo\n");
		Close(connfd);
	
		exit(0);
}

void echo(int connfd)
{
	size_t no;
	char buf[MAXLINE];
	rio_t rio;
	
	
	Rio_readinitb(&rio, connfd);
	int n=read(connfd,buf,MAXLINE);
	printf("%d\n",n );
	printf("El archivo a buscar es: %s\n",buf );
	char * mensaje;
	FILE *infile;
	int numbytes;

	infile = fopen(buf, "r+");
	if(infile!=NULL){
 		
		/* open an existing file for reading */
		/* Get the number of bytes */
		fseek(infile, 0L, SEEK_END);
		numbytes = ftell(infile) ;
		 
		//char buffer[numbytes];
		char*buffer = (char*)calloc(numbytes, sizeof(char));	


		/* reset the file position indicator to 
		the beginning of the file */
		fseek(infile, 0L, SEEK_SET);	
		 
		
		/* memory error */
		if(buffer != NULL){
			/* copy all the text into the buffer */
			int bytesleidos=fread(buffer, sizeof(char), numbytes, infile);
			fclose(infile);
			 
			/* confirm we have read the file by
			outputing it to the console */
			printf("The file called contains this num of bytes %d\n\n%s", bytesleidos);
			write(connfd,buffer,strlen(buffer));
			printf("%s\n", buffer);
			free(buffer);
		}
		else{
			mensaje="No hay nada en el archivo";
			write(connfd,mensaje,strlen(mensaje));
			printf("%s\n",mensaje );

		}

	}

	else{
		mensaje="no existe el archivo";
		write(connfd,mensaje,strlen(mensaje));
		printf("%s\n",mensaje );
	}
	memset(buf, 0, sizeof buf);

	//Rio_writen(connfd, buf, n);

	// if((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0){
	// 	printf("server received %lu bytes\n", no);
	// 	Rio_writen(connfd, buf, n);
	// }

	
	// while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
	// 	printf("server received %lu bytes\n", n);
	// 	Rio_writen(connfd, buf, n);
	// }
}
